import React from "react";

// Material-ui
import Box from "@material-ui/core/Box";

// CSS
import "../styles/styleCategory.css";

export default function ItemCategory(props: any) {
  // const classes = useStyles();

  return (
    <Box
      className="conCategory"
      onClick={() => alert(props.data.category_name)}
    >
      <div className="txtCategoty">{props.data.category_name}</div>
    </Box>
  );
}
