import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

// Screen
import ItemCategory from "./ItemCategory";

// CSS
import "../styles/styleCategory.css"

// DATA
import dataCategory from "../dataCategory";

export default function SlideCategory(props: any) {
  const data_category = dataCategory.result.data.category_list;
  const itemList = [];
  let start = props.start - 4;
  let sed = 4 - props.sed;

  switch (sed) {
    case 1:
      for (let i = start; i < start + 1; i++) {
        itemList.push(<ItemCategory key={i} data={data_category[i]} />);
      }
      break;
    case 2:
      for (let i = start; i < start + 2; i++) {
        itemList.push(<ItemCategory key={i} data={data_category[i]} />);
      }
      break;
    case 3:
      for (let i = start; i < start + 3; i++) {
        itemList.push(<ItemCategory key={i} data={data_category[i]} />);
      }
      break;
    default:
      for (let i = start; i < start + 4; i++) {
        itemList.push(<ItemCategory key={i} data={data_category[i]} />);
      }
  }

  return (
    <div>
      <Box className="containerBox">{itemList}</Box>
    </div>
  );
}
