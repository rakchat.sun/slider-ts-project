import React from "react";

// Material-ui
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

// Screen
import ItemTopten from "./ItemTopten";

// CSS
import "../styles/styleTopten.css";

// DATA
import dataTopten from "../dataTopten";

const useStyles = makeStyles({
  containerBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
    // marginBlock: "10px",
  },
});

export default function SlideTopten(props: any) {
  const classes = useStyles();
  const data_service = dataTopten.result.data.all_service_item;
  const itemList = [];
  let start = props.start - 4;
  let sed = 4 - props.sed;

  switch (sed) {
    case 1:
      for (let i = start; i < start + 1; i++) {
        itemList.push(<ItemTopten key={i} data={data_service[i]} />);
      }
      break;
    case 2:
      for (let i = start; i < start + 2; i++) {
        itemList.push(<ItemTopten key={i} data={data_service[i]} />);
      }
      break;
    case 3:
      for (let i = start; i < start + 3; i++) {
        itemList.push(<ItemTopten key={i} data={data_service[i]} />);
      }
      break;
    default:
      for (let i = start; i < start + 4; i++) {
        itemList.push(<ItemTopten key={i} data={data_service[i]} />);
      }
  }
  return (
    <Box className="container">
      <Box className={classes.containerBox}>{itemList}</Box>
    </Box>
  );
}
