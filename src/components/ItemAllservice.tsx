import React from "react";

// Material-ui
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

// CSS
import "../styles/styleAllservice.css";

const useStyles = makeStyles({
  conService: {
    width: "40%",
    height: "110px",
  },
  circleImage: {
    width: "130px",
    height: "130px",
    borderRadius: "50%",
    position: "absolute",
  },
  boxContent: {
    backgroundColor: "#fabd23",
    width: "80%",
    right: "0",
    top: "14%",
    height: "72%",
    position: "absolute",
    borderRadius: "20px",
    boxShadow: "0px 2px 5px grey",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    position: "relative",
    fontSize: "18px",
    wordWrap: "break-word",
    height: "100%",
  },
  btnPayNow: {
    color: "white",
    padding: "4px 20px 4px 20px",
    backgroundColor: "black",
    borderRadius: "25px",
    position: "absolute",
    bottom: "20%",
    right: "10%",
    fontWeight: "bold",
    "&:hover": {
      background: "black",
    },
  },
});

export default function ItemAllservice(props: any) {
  const classes = useStyles();
  // mb={2} mt={2.5} mr={2} ml={2}
  return (
    <Box mr={8} mb={2} mt={2.5} className={classes.conService}>
      <Box className="conTopTen" onClick={() => alert(props.data.service_name)}>
        <Box className={classes.circleImage}>
          <img
            style={{ zIndex: 1 }}
            className={classes.circleImage}
            src="https://jaksestudio.com/wp-content/uploads/2020/03/JAKS-estudio-Womens-Haircut.png"
            alt="new"
          />
        </Box>
        <Box className={classes.boxContent}>
          <Box mt={1} ml={10} pr={1.25} className={classes.content}>
            <Typography
              className="item_name"
              style={{ fontWeight: "bold", fontSize: "16px" }}
            >
              {props.data.service_name}
            </Typography>
            <Typography style={{ color: "#7E7E7E", fontWeight: "bold" }}>
              ({props.data.category_name})
            </Typography>
            <Box mt={0.125}>
              <Typography variant="h6" style={{ fontWeight: "bold" }}>
                {props.data.service_price} ฿
              </Typography>
            </Box>
            <button className="btnPayNow">Pay Now</button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
