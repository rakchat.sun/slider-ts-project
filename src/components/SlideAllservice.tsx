import React from "react";

// Material-ui
import { makeStyles } from "@material-ui/core/styles";
// import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";

// Screen
import ItemAllservice from "./ItemAllservice";

// CSS
import "../styles/styleAllservice.css";

// DATA
import dataAllservice from "../dataAllservice";

const useStyles = makeStyles({
  containerBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
});

export default function SlideAllservice(props: any) {
  const classes = useStyles();
  const data_service = dataAllservice.result.data.all_service_item;
  const itemList = [];
  let start = props.start - 8;
  let sed = 8 - props.sed;
  console.log("item sed: " + sed);

  switch (sed) {
    case 1:
      for (let i = start; i < start + 1; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 2:
      for (let i = start; i < start + 2; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 3:
      for (let i = start; i < start + 3; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 4:
      for (let i = start; i < start + 4; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 5:
      for (let i = start; i < start + 5; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 6:
      for (let i = start; i < start + 6; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    case 7:
      for (let i = start; i < start + 7; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
      break;
    default:
      for (let i = start; i < start + 8; i++) {
        itemList.push(<ItemAllservice key={i} data={data_service[i]} />);
      }
  }
  return (
    <Box className="container">
      <Box className={classes.containerBox}>{itemList}</Box>
    </Box>
  );
}
