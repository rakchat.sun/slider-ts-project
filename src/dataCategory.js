const data = {
  message_error: null,
  result: {
    data: {
      category_list: [
        {
          category_name: "Hair Design",
          category_id: 1,
        },
        {
          category_name: "Hair Chemical",
          category_id: 2,
        },
        {
          category_name: "Skins Service",
          category_id: 3,
        },
        {
          category_name: "Nail Services",
          category_id: 4,
        },
        {
          category_name: "Aa Services",
          category_id: 5,
        },
        {
          category_name: "Bb Services",
          category_id: 6,
        },
        {
          category_name: "Cc Services",
          category_id: 7,
        },
        {
          category_name: "Dd Services",
          category_id: 8,
        },
        {
          category_name: "Ee Services",
          category_id: 9,
        },
        {
          category_name: "Ff Services",
          category_id: 10,
        },
      ],
    },
  },
  status: "success",
  status_code: 200,
};

export default data;
