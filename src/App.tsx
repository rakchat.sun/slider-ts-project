import React from "react";

// CSS
import "swiper/swiper-bundle.css";
import "./styles/styleCategory.css";
import "./styles/styleTopten.css";
import "./styles/styleAllservice.css";

// Data and Item category
import dataCategory from "./dataCategory";
import SlideCategory from "./components/SlideCategory";

// Data and Item topten
import dataTopten from "./dataTopten";
import SlideTopten from "./components/SlideTopten";

// Data and Item topten
import dataAllservice from "./dataAllservice";
import SlideAllservice from "./components/SlideAllservice";

// Swiper or Slider
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination, Controller, Thumbs } from "swiper";
SwiperCore.use([Navigation, Pagination, Controller, Thumbs]);

export default function App() {
  const slidesTopten = [];
  let size_data = dataTopten.result.data.all_service_item.length;
  let start = 0;
  let sed = 0;

  for (let i = 0; i < size_data / 4; i += 1) {
    start = start + 4;
    sed = start - size_data;
    slidesTopten.push(
      <SwiperSlide key={`slide-${i}`} tag="li">
        <SlideTopten start={start} sed={sed} />
      </SwiperSlide>
    );
  }

  const slideCategory = [];
  let size_dataCategory = dataCategory.result.data.category_list.length;
  let startCategory = 0;
  let sedCategory = 0;

  for (let i = 0; i < size_dataCategory / 4; i += 1) {
    startCategory = startCategory + 4;
    sedCategory = startCategory - size_dataCategory;
    slideCategory.push(
      <SwiperSlide key={`slide-${i}`} tag="li">
        <SlideCategory start={startCategory} sed={sedCategory} />
      </SwiperSlide>
    );
  }

  const slidesAllservice = [];
  let size_dataAllservice = dataAllservice.result.data.all_service_item.length;
  let startAllservice = 0;
  let sedAllservice = 0;

  for (let i = 0; i < size_dataAllservice / 8; i += 1) {
    startAllservice = startAllservice + 8;
    sedAllservice = startAllservice - size_dataAllservice;
    slidesAllservice.push(
      <SwiperSlide key={`slide-${i}`} tag="li">
        <SlideAllservice start={startAllservice} sed={sedAllservice} />
      </SwiperSlide>
    );
  }

  return (
    <div>
      <div
        style={{ paddingLeft: "50px", paddingRight: "50px", marginTop: "10px" }}
      >
        <img
          style={{
            width: "100%",
            height: "160px",
            borderRadius: "24px",
            marginBottom: "10px",
          }}
          src="https://images.unsplash.com/photo-1600948835780-9c4a8b55cf50?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
          alt="new"
        />
      </div>
      <Swiper id="category" tag="section" navigation pagination>
        {slideCategory}
      </Swiper>
      <br />
      <h2 style={{ marginLeft: "50px" }}>Top 10 Service</h2>
      <Swiper id="topten" tag="section" navigation pagination>
        {slidesTopten}
      </Swiper>
      <br />
      <h2 style={{ marginLeft: "50px" }}>All Service</h2>
      <Swiper id="allservice" tag="section" navigation pagination>
        {slidesAllservice}
      </Swiper>
    </div>
  );
}
